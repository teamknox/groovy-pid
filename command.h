/**
 * @file      command.h
 * @author    Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * Groovy-PID/Quatro (Version 1.2.0)
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef COMMAND_H
#define	COMMAND_H


#include "MS_lib/core/microshell.h"
#include "MS_lib/core/msconf.h"
#include "MS_lib/util/mscmd.h"

extern void utx(char c);
extern char urx(void);
extern void action_hook(MSCORE_ACTION action);

typedef struct {
    void (*puts)(const char *str);
} USER_OBJECT;

USER_OBJECT usrobj = {
    .puts = myPuts,
};

extern void ResetScreen();

extern MSCMD_USER_RESULT usrcmd_LED(MSOPT *msopt, MSCMD_USER_OBJECT usrobj);
extern MSCMD_USER_RESULT usrcmd_Counter(MSOPT *msopt, MSCMD_USER_OBJECT usrobj);
extern MSCMD_USER_RESULT usrcmd_ClearScreen(MSOPT *msopt, MSCMD_USER_OBJECT usrobj);
extern MSCMD_USER_RESULT usrcmd_Speed(MSOPT *msopt, MSCMD_USER_OBJECT usrobj);
extern MSCMD_USER_RESULT usrcmd_Brake(MSOPT *msopt, MSCMD_USER_OBJECT usrobj);
extern MSCMD_USER_RESULT usrcmd_GateTime(MSOPT *msopt, MSCMD_USER_OBJECT usrobj);
extern MSCMD_USER_RESULT usrcmd_ConstantP(MSOPT *msopt, MSCMD_USER_OBJECT usrobj);
extern MSCMD_USER_RESULT usrcmd_ConstantN(MSOPT *msopt, MSCMD_USER_OBJECT usrobj);

extern MSCMD_USER_RESULT usrcmd_Position(MSOPT *msopt, MSCMD_USER_OBJECT usrobj);
extern MSCMD_USER_RESULT usrcmd_Mode(MSOPT *msopt, MSCMD_USER_OBJECT usrobj);
extern MSCMD_USER_RESULT usrcmd_TA(MSOPT *msopt, MSCMD_USER_OBJECT usrobj);
extern MSCMD_USER_RESULT usrcmd_Echo(MSOPT *msopt, MSCMD_USER_OBJECT usrobj);
extern MSCMD_USER_RESULT usrcmd_Frequency(MSOPT *msopt, MSCMD_USER_OBJECT usrobj);
extern MSCMD_USER_RESULT usrcmd_Debug(MSOPT *msopt, MSCMD_USER_OBJECT usrobj);
extern MSCMD_USER_RESULT usrcmd_Version(MSOPT *msopt, MSCMD_USER_OBJECT usrobj);

extern MSCMD_USER_RESULT usrcmd_I2C(MSOPT *msopt, MSCMD_USER_OBJECT usrobj);
extern MSCMD_USER_RESULT usrcmd_Help(MSOPT *msopt, MSCMD_USER_OBJECT usrobj);

extern MSCMD_COMMAND_TABLE table[] = {

    {   "L",   usrcmd_LED   },
    {   "l",   usrcmd_LED   },

    {   "C",   usrcmd_Counter   },
    {   "c",   usrcmd_Counter   },
    
    {   "cls",   usrcmd_ClearScreen   },

    {   "S",   usrcmd_Speed   },
    {   "s",   usrcmd_Speed   },

    {   "B",   usrcmd_Brake   },
    {   "b",   usrcmd_Brake   },

    {   "G",   usrcmd_GateTime   },
    {   "g",   usrcmd_GateTime   },

    {   "kp",   usrcmd_ConstantP   },
    
    {   "kn",   usrcmd_ConstantN   }, 
    
    {   "P",   usrcmd_Position   },
    {   "p",   usrcmd_Position   },    

    {   "M",   usrcmd_Mode   },
    {   "m",   usrcmd_Mode   },

    {   "T",   usrcmd_TA   },
    {   "t",   usrcmd_TA   },

    {   "E",   usrcmd_Echo   },
    {   "e",   usrcmd_Echo   },

    {   "F",   usrcmd_Frequency   },
    {   "f",   usrcmd_Frequency   },
    
    {   "D",   usrcmd_Debug   },
    {   "d",   usrcmd_Debug   },
    
    {   "V",   usrcmd_Version   },
    {   "v",   usrcmd_Version   },
    
// Reserved for following commands    
    {   "I",   usrcmd_I2C   },
    {   "i",   usrcmd_I2C   },
    
    {   "?",   usrcmd_Help   },
    {   "help",   usrcmd_Help   },
};

#endif	/* COMMAND_H */

