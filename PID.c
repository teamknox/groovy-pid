/**
 * @file      PID.c
 * @author    Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * Groovy-PID/Quatro (Version 1.2.0)
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */


#include <xc.h>
#include <PIC18.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "io_define.h"
#include "PID.h"
#include "UART.h"
#include "channel.h"
#include "eeprom.h"
#include "channel.h"
#include "supplement.h"

#define CONTROL_THEORY_SELECT 0

extern volatile long gCountEnc[MAX_CHANNEL];
extern volatile long gLastCountEnc[MAX_CHANNEL];
extern unsigned char gFlgControllingPosition;
extern unsigned char gFlgControlLoop;
extern unsigned char gFlgDebug;
extern int gDesiredTarget[MAX_CHANNEL];
extern volatile long gDiffCount[MAX_CHANNEL];

extern volatile long gTimer1ms;


void PID(void)
{
//    PidSpeed();
    PidPosition();
}

void PidSpeed(void)
{
    long operation;
    unsigned char i, j, min, flgChPos = 0b00000001;;

    if(gTimer1ms % 100 == 0){
        for (i = 0;i < USED_CHANNEL;i++){
            GIEH = 0;   // Disable interrupts so we don't get back reference data
            gDiffCount[i] = gCountEnc[i] - gLastCountEnc[i];
            gLastCountEnc[i] = gCountEnc[i];
            GIEH = 1;   // Re-enable interrupts
        }
        min = MinIndex(USED_CHANNEL, gDiffCount);
        flgChPos = flgChPos << min;
        
        for(i = 0;i < USED_CHANNEL;i++){
            gError[i] = gDiffCount[i] - gDiffCount[min];
            if(gDesiredTarget[i] >= 0)
                j = 1;
            else
                j = 0;
//          gErrorIntegral[i] += gError[i]; // Usually this ?
            gErrorIntegral[i] = gLastError[i] + gError[i];
            
            operation = gP_gain[j] * gError[i] + gI_gain[j] * gErrorIntegral[i] + gD_gain[j] * (gError[i] - gLastError[i]);
            printf("%d ", operation);
/*
            if((gDesiredTarget[i] >= 0) && (operation > gDesiredTarget[i])){
                operation = gDesiredTarget[i];
                SetForward(i);
            }
            if((gDesiredTarget[i] < 0) && (operation < gDesiredTarget[i])){
                operation = gDesiredTarget[i];
                SetBackward(i);
            }
            SetPWM(i, abs(operation));
*/
            gLastError[i] = gError[i];

        }
//        printf("\r\n");
//        printf("%d %d %d %d\r\n", gDiffCount[0], gDiffCount[1], gDiffCount[2], gDiffCount[3]);
//        printf("%d\r\n", gTimer1ms);
    }
}

void PidPosition(void)
{
    long operation;
    unsigned char i, j, flgChPos = 0b00000001;
    
//    if(gFlgControllingPosition){
        for (i = 0;i < USED_CHANNEL;i++){
            GIEH = 0;   // Disable interrupts so we don't get back reference data
            gError[i] = gTickTarget[i] - gCountEnc[i];
            GIEH = 1;   // Re-enable interrupts
        }
        for (i = 0;i < USED_CHANNEL;i++){
            if(bittst(gFlgControllingPosition, flgChPos)){
                if(gDesiredTarget[i] >= 0)
                    j = 1;
                else
                    j = 0;
            
    
#if CONTROL_THEORY_SELECT
                switch(gFlgControlLoop){
                    case kP_Control:
                        operation = gP_gain[j] * gErrorLeft;
                        break;
                    case kPI_Control:
//                      gErrorIntegral[i] += gError[i]; // Usually this ?
                        gErrorIntegral[i] = gLastError[i] + gError[i];
                        operation = gP_gain[j] * gError[i] + gI_gain[j] * gErrorIntegral[i];
                        break;
                    case kPD_Control:
                        operation = gP_gain[j] * gError[i] + gD_gain[j] * (gError[i] - gLastErro[i]);
                        break;
                    case kPID_Control:
//                      gErrorIntegral[i] += gError[i]; // Usually this ?
                        gErrorIntegral[i] = gLastError[i] + gError[i];
                        operation = gP_gain[j] * gError[i] + gI_gain[j] * gErrorIntegral[i] + gD_gain[j] * (gError[i] - gLastError[i]);
                        break;
                    default:
                        break;
                }
#else
//              gErrorIntegral[i] += gError[i]; // Usually this ?
                gErrorIntegral[i] = gLastError[i] + gError[i];
            
                operation = gP_gain[j] * gError[i] + gI_gain[j] * gErrorIntegral[i] + gD_gain[j] * (gError[i] - gLastError[i]);
#endif
                if((gDesiredTarget[i] >= 0) && (operation > gDesiredTarget[i])){
                    operation = gDesiredTarget[i];
                    SetForward(i);
                }
                if((gDesiredTarget[i] < 0) && (operation < gDesiredTarget[i])){
                    operation = gDesiredTarget[i];
                    SetBackward(i);
                }
                SetPWM(i, abs(operation));
                gLastError[i] = gError[i];
            }
//            flgChPos = flgChPos << 1;
        }
//    }
}


void SetPosition(unsigned char aCh, long aNewTarget)
{
    unsigned char flgChPos = 0b00000001;

    gCountEnc[aCh] = 0;
    gLastCount[aCh] = 0;
    gErrorIntegral[aCh] = 0;
    gLastError[aCh] = 0;

    gTickTarget[aCh] = aNewTarget;
    
    flgChPos = flgChPos << aCh;
    bitset(gFlgControllingPosition, flgChPos);   
}

unsigned char MinIndex(unsigned char aLen, long *aNums) 
{
    unsigned char j;
    int i;
    long _min = aNums[0];
    for (i = 1; i < aLen; i++) {
        if (aNums[i] < _min)
            j = i;
        _min = aNums[i];
    }
    return j;
}
