/* 
 * File:   18F26K22_config.h
 * Author: ooo
 *
 * Created on September 23, 2019, 1:47 PM
 */

#ifndef MCU_CONFIG_H
#define	MCU_CONFIG_H

// For configuration setting
// No External-clock, No-PLL, Internal clock
#pragma config PRICLKEN=OFF,PLLCFG=OFF,FOSC=INTIO67  // CONFIG1H

// Brown-Out-Reset enable, MonitorVoltage=2.85V
// Start after Power=on (65.6ms)
#pragma config BOREN=NOSLP,BORV=285,PWRTEN=ON        // CONFIG2L

// No WDT 
#pragma config WDTEN=OFF                             // CONFIG2H

// No-External-Reset (MCLR_INTMCLR)
// System-clock supplying after OSC-stabled
#pragma config MCLRE=EXTMCLR,HFOFST=OFF              // CONFIG3H

// No-LowVoltage-programming (LVP_OFF)
#pragma config LVP=OFF                               // CONFIG4L

// CCP2 handling for Legacy(Standard) PWM using
#pragma config CCP2MX=PORTC1




#endif	/* MCU_CONFIG_H */

