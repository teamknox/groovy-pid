/**
 * @file      eeprom.c
 * @author    Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * Groovy-PID/Quatro (Version 1.2.0)
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include "eeprom.h"
#include "io_define.h"

// Read EEPROM
unsigned char eep_read(unsigned char aEadd)
{
    unsigned char edata;
    
    EEADR = aEadd;      // Set Read Address 
    EECON1 = 0;         // Init control-register
    EECON1bits.RD = 1;  // Start Read
    while(EECON1bits.RD == 1);
    edata = EEDATA;     // Store read data
    return edata;
}

void eep_write(unsigned char aEadd, unsigned char aEdata)
{
    INTCONbits.GIE = 0;         // Disable All-interrupt
    EEADR = aEadd;              // Set write address
    EEDATA = aEdata;            // Set write data
    EECON1 = 0;                 // Init control-register
    EECON1bits.WREN = 1;        // Enable eeprom write
    EECON2 = 0x55;              // Start command #1
    EECON2 = 0xaa;              // Start command #2
    EECON1bits.WR = 1;          // Start write
    while(EECON1bits.WR == 1);
    INTCONbits.GIE = 1;         // Enable all-interrupt 
}

void write_int_to_eeprom(unsigned char aEadd, int aData)
{
    unsigned int i;

    char *pointer, byte;
    
    pointer = (char *)&aData;
    for(i = 0; i < sizeof(aData); i++){
        byte = pointer[i];
        eep_write(aEadd + i, byte);
    }

}

void read_int_from_eeprom(unsigned char aEadd, int *aData)
{
    unsigned int i;
    int temp;
    char *pointer;
    
    pointer = (char *)&temp;
    for(i = 0; i < sizeof(temp); i++){
        pointer[i] = eep_read(aEadd + i);
    }
    
    *aData = temp;
}

void write_float_to_eeprom(unsigned char aEadd, float aData)
{
    unsigned int i;

    char *pointer, byte;
    
    pointer = (char *)&aData;
    for(i = 0; i < sizeof(aData); i++){
        byte = pointer[i];
        eep_write(aEadd + i, byte);
    }

}

void read_float_from_eeprom(unsigned char aEadd, float *aData)
{
    unsigned int i;
    float temp;
    char *pointer;
    
    pointer = (char *)&temp;
    for(i = 0; i < sizeof(temp); i++){
        pointer[i] = eep_read(aEadd + i);
    }
    
    *aData = temp;
}

