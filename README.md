# Groovy-PID
Groovy-PID is a PID controller sample firmware for PIC18F26K22, designed and implemented for [Groovy-PID](http://www.omiya-giken.com/?page_id=2681).
Mainly two type of controller such as "Open Loop" and "Closed loop".

<img src="/uploads/1b2e9545761c10a23af564cffd8fe3d3/IMG_2085.jpg" width="240" height="320">
<img src="/uploads/3e2e349c3e00b31eff1c082c606783ad/IMG_2086.jpg" width="320" height="240">


# How to build
The repository snapshot for MPLAB XIDE. 1stly, you should make project, then copied each file to mean project.


# How to use
- Detail is described in [Japanese](http://www.omiya-giken.com/?page_id=2681).
- Detail is described in [English](http://www.omiya-giken.com/?page_id=2681&lang=en).


# What's the application ?
Crawler / Wheel type rover (robot) are typical application as below,  

## Triliobite
 <p>
 <img src="/uploads/106c8a8fae78c1f025f18bee851296b6/IMG_2131.jpg" width="320" height="240">
 </p>

# License
Copyright (c) Osamu OHASHI  
Distributed under the MIT License either version 1.0 or any later version. 